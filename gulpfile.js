/**
 * Gulpfile Setup
 * author: Raúl Hernández <raulghm@gmail.com>
 */

 'use strict';

/**
 * Settings
 */

var gulp = require('gulp'),
$ = require('gulp-load-plugins')(),
runSequence = require('run-sequence'),
del = require('rimraf'),

SRC = './src/',
DEST = './dist/',

vars = {
	name: require('./package.json').name,
	authors: require('./package.json').authors,
	version: require('./package.json').version,
	homepage: require('./package.json').homepage,
	description: require('./package.json').description,
	iconsGlyph: require(SRC + '/data/glyph.json'),
	iconsOutline: require(SRC + '/data/outline.json'),
	iconsMini: require(SRC + '/data/mini.json'),
	prefix: 'nc',
	date: new Date()
},
template = {
	name: vars.name,
	version: vars.version,
	iconsOutline: vars.iconsOutline,
	iconsGlyph: vars.iconsGlyph,
	iconsMini: vars.iconsMini,
	prefix: vars.prefix,
	comments: ['/*!',
	'	' + vars.name + ' ' + vars.version,
	'	' + vars.homepage,
	'	Licensed under the MIT license - http://opensource.org/licenses/MIT',
	'	Copyright (c) ' + vars.date.getFullYear() + ' Raul Hernandez',
	'*/'].join('\n')
};

/**
 * Tasks
 */

// Clean up
gulp.task('clean', del.sync(DEST));

// Fonts
gulp.task('fonts', function () {
	return gulp.src(SRC + 'fonts/**/*')
	.pipe(gulp.dest(DEST + 'fonts'));
});

// Styles
gulp.task('styles', function () {
	return gulp.src(SRC + 'scss/nucleo-icons.scss')
	.pipe($.sass())
	// .pipe($.cssnano())
	.pipe(gulp.dest(DEST + 'styles'));
});

// Test
gulp.task('html', function () {
	return gulp.src(SRC + 'html/*.html')
	.pipe($.data(function() {
		return template;
		}))
	.pipe($.template())
	.pipe(gulp.dest(DEST));
});

// Copy
gulp.task('copy', function () {
	return gulp.src(SRC + 'scss/**/*')
	.pipe(gulp.dest(DEST + 'scss'));
});

// Default
gulp.task('default', ['clean'], function () {
	runSequence([
		'fonts',
		'styles',
		'html',
		'copy',
	]);
});
