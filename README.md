# Nucleo icons
> v2.7.1

##Para compilar
```
npm run dist
```

###Instalación
```css
@import '../bower_components/nucleo-icons/dist/scss/nucleo-icons';
```

###Uso html
```html
<i class="nc-GesturesGrab"></i>
```

###Uso SCSS
```css
.div {
	@include nc(GesturesGrab);
}
```
